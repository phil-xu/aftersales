var express = require('express');
var path = require('path');
// var favicon = require('serve-favicon');
// var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var compress = require('compression');


var models = require("./models");
var http = require('http');
app.use(compress());
// view engine setup
// app.set('views', path.join(__dirname, 'dist'));
// // app.set('view engine', 'jade');
// app.engine('.html', require('ejs').renderFile);  
// app.set('view engine', 'html');  
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


// var str = '4d056a5705a5297379dcff80ba052390' + 'appkey23307822instance_id121ts1473061265514' + '4d056a5705a5297379dcff80ba052390';
// var hmac = crypto.createHmac('md5', '4d056a5705a5297379dcff80ba052390');
// hmac.update(str);
// var _sign = hmac.digest('hex');
// console.log(_sign);


// 跨域支持
app.all('/*', function (req, res, next) {
    
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE');

  next();
});
// app.all('/*', function (req, res, next) {
    
//   var ts = req.query.ts;
  
//   // if(!ts || ts < (new Date().getTime()) - 6 * 60* 60) {
//   //   res.sendStatus(403);
//   //   return;
//   // }
  
//   var appkey = req.query.appkey;
//   var instance_id = req.query.instance_id;
//   var sign = req.query.sign;
//   var str = '4d056a5705a5297379dcff80ba052390' + 'appkey23307822instance_id121ts1472005739217' + '4d056a5705a5297379dcff80ba052390';
//   var hmac = crypto.createHmac('md5', '23307822');
//   hmac.update(str);
//   var _sign = hmac.digest('hex');
//   console.log(_sign);

//   next();
// });
// app.use('/', function(req, res) {
//     res.sendStatus(200);
// });

app.use('/api', require('./api'));
app.use(express.static(path.join(__dirname, 'html'), {maxAge: '365d'}));


// app.get('/**', function(req, res) {
//     res.render('index');
// });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    error: (app.get('env') === 'development') ? err : {}
  });
});





var properties = require ("properties");



var server = http.createServer(app);
models.sequelize.sync().then(function () {
  var port = process.env.PORT || 3006;


  console.log('listening at :%s', port);
  server.listen(port);


 
});

