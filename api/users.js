var models  = require('../models');
var express = require('express');
var router = express.Router();
var request = require('request')
router.get('/', function(req, res, next) {
    models.User.findAll({
    }).then(function(users) {
        if(users.length) res.send(users);
        else res.sendStatus(404);
    });
});

module.exports = router;
