var WANTU = require('wantu-nodejsSDK');
var wantu = new WANTU('23307609', 'dc81271da14fa6bba02a91a9e9d57b61');
var express = require('express');
var fs = require('fs');
var md5 = require('md5');
var del = require('del');
var formidable = require('formidable');
var router = express.Router();
var STATIC = 'upload/'
var AVATAR_UPLOAD_FOLDER = 'images/'
router.get('/upload', function(req, res, next) {
	res.send({1: 2})
});

router.post('/upload', function(req, res, next) {
	var time = Date.now()
	var form = new formidable.IncomingForm();
	form.uploadDir = STATIC + AVATAR_UPLOAD_FOLDER; 
	form.parse(req, function(error, fields, files) {
    	console.log('error: ',error)
        console.log("parsing done");
        console.log(files);
        console.log(files.file.path);
        
        var data = fs.readFileSync(files.file.path);
        var filename = md5(data);
        var ext = files.file.type == 'image/png' ? '.png' : '.jpg';
        var filepath = form.uploadDir + filename + ext;
        console.log('filename: ', filepath)
        fs.renameSync(files.file.path, filepath);
        wantu.singleUpload({
			insertOnly: 0,
			namespace: 'malianghang',
			expiration: -1
		}, filepath, 'product/images', '', '', function(err, response) {
			if (err) {
				res.status(408);
				res.send(err);
			} else {
				del.sync([filepath]);
				var data = JSON.parse(response.data);
				res.send({
					image: data.url
				});
			}
		})
  	});
});

module.exports = router;
