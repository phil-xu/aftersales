var express = require('express');
var router  = express.Router();
router.use('/records', require('./records'));
router.use('/images', require('./images'));
router.use('/files', require('./files'));
router.use('/users', require('./users'));
module.exports = router;