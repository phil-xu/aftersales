var models  = require('../models');
var express = require('express');
var router = express.Router();
var request = require('request')
var Excel = require('exceljs');
var fs = require('fs');
var download = require('download');
var Sequelize = require("sequelize");

function insertImage (workbook, worksheet, url, i) {
    return download(url).then(data => {
        var imageId2 = workbook.addImage({
          buffer: data,
          extension: 'png',
        });

        worksheet.addImage(imageId2, {
          tl: { col: 0, row: i + 1 },
          br: { col: 1, row: i + 2 }
        });
        
    })
}

function exportExcel(records, res) {
    var workbook = new Excel.Workbook();
    var worksheet = workbook.addWorksheet('My Sheet');

    worksheet.columns = [
        { header: '缩率图', key: '', width: 20 },
        { header: '状态', key: 'status', width: 20 },
        { header: '收到日期', key: 'recieveDate', width: 10 },
        { header: '名称', key: 'name', width: 30 },
        { header: '材质', key: 'material', width: 10 },
        { header: '定制内容', key: 'content', width: 10 },
        { header: '来源', key: 'platform', width: 10 },
        { header: '单号', key: 'orderId', width: 30 },
        { header: '顾客', key: 'customer', width: 10 },
        { header: '件数', key: 'quantity', width: 10 },
        { header: '退货单号', key: 'recieveId', width: 20 },
        { header: '售后详情', key: 'reason', width: 10 },
        { header: '售后类型', key: 'type', width: 10 },
        { header: '改圈量', key: 'resize', width: 10 },
        { header: '登记日期', key: 'createdAt', width: 10 },
        { header: '下单日期', key: 'orderDate', width: 10 },
        { header: '发货信息', key: 'address', width: 100 },
        { header: '发货单号', key: 'sendId', width: 20 },
        { header: '发货日期', key: 'sendDate', width: 10 },
        { header: '备注', key: 'memo', width: 20 },
    ];
    var list = []
    for (var i = 0; i < records.length; i++) {
        var record = records[i].get({
            plain: true
        })
        console.log(record)
        var statusMap = ['登记未寄回', '寄回售后中', '寄回未登记', '售后已完成', '登记未补发']
        record.status = statusMap[parseInt(record.statusCode) - 1]
        var row = worksheet.addRow(record);
        row.height = 80;
        if (record.thumb) list.push(insertImage(workbook, worksheet, record.thumb, i))
    }
    

    return Promise.all(list).then(() => {
        var date = new Date()
        var today = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader('Content-Disposition', 'attachment; filename=after-sales-' + today + '.xlsx');
        return workbook.xlsx.write(res)
        
    })
}

function queryRecords (req) {
    var name = req.query.name
    var limit = req.query.pageSize ? parseInt(req.query.pageSize) : 10
    var offset = req.query.currentPage ? (parseInt(req.query.currentPage) -1) * limit : 0
    var orderCode = req.query.order == 'descending' ? 'DESC' : 'ASC'

    var where = {}
    var order = [
        ['id', 'ASC']
    ]



    if (req.query.name) {
        where['$or'] = {
            name: {
                $like: '%' + req.query.name + '%'
            },
            orderId: {
                $like: '%' + req.query.name + '%'
            },
            address: {
                $like: '%' + req.query.name + '%'
            },
            reason: {
                $like: '%' + req.query.name + '%'
            },
            customer: {
                $like: '%' + req.query.name + '%'
            },
            platform: {
                $like: '%' + req.query.name + '%'
            },
            recieveId: {
                $like: '%' + req.query.name + '%'
            }
        }
    }
    if (req.query.types) {
        var types = req.query.types instanceof Array ? req.query.types : [req.query.types]
        where['type'] = {
            $in: types
        }
    }
    if (req.query.status) {
        var statusStringList = req.query.status instanceof Array ? req.query.status : [req.query.status]
        var statusCodes = []
        for (var i = 0; i < statusStringList.length; i++) {
            if (statusStringList[i])
            statusCodes.push(parseInt(statusStringList[i]))
        }
        where['statusCode'] = {
            $in: statusStringList
        }
    }
    if (req.query.orderProp == 'recieveDate') {
        order.unshift(['recieveDate', orderCode])
    } else {
        order.unshift(['id', orderCode])
    }
    console.log('where: ', where)
    return models.Record.findAndCountAll({
        where: where,
        order: order,
        limit: limit,
        offset: offset
        
    })
}

router.get('/export', function(req, res, next) {
    queryRecords(req).then(function(results) {
        return exportExcel(results.rows, res).then(function() {
            res.end();
        })
    }).catch(function(e){
        res.send(e);
    });
});


router.get('/:id', function(req, res, next) {
    var id = req.params.id;
    var attributes = 
    models.Record.find({
        where: {
            id: id
        }
    }).then(function(record) {
        if(record) {
            console.log(record)
            res.send(record);
        } else {
            res.send({errorCode: 404});
        }
    }).catch(function(e){
        res.send(e);
    });
});

router.get('/', function(req, res, next) {
    queryRecords(req).then(function(records) {
        console.log('records: ', records)
        res.send(records);
    }).catch(function(e){
        res.send(e);
    });
});
router.post('/', function(req, res, next) {
    console.log(2)
    models.Record.create(req.body).then(function(newPage) {
        res.send(newPage);
    }).catch(function(e){
        res.send(e);
    });
});
router.delete('/:id', function(req, res) {
    var id = req.params.id;
    models.Record.destroy({
        where: {
            id: id
        }
    }).then(function() {
        res.send({});
    });
});
router.post('/:id', function(req, res, next) {
    var id = req.params.id;
    models.Record.find({
        where: {
            id: id
        }
    }).then(function(record) {
        if(record) {
            
            var copyRecord = record.get({
                plain: true
            })
            console.log('record: ', copyRecord)
            delete copyRecord.id
            models.Record.create(copyRecord).then(function(newPage) {
                res.send(newPage);
            }).catch(function(e){
                res.send(e);
            });
        } else {
            res.send({errorCode: 404});
        }
    }).catch(function(e){
        res.send(e);
    });
    
});

router.put('/:id', function(req, res) {
    var id = req.params.id;
    var obj = req.body;
    console.log(obj)
    models.Record.find({
        where: {
            id: id
        }
    }).then(function(record) {
        if(record) {
            record.updateAttributes(obj).then(function(newRecord){
                res.send(newRecord);
            })
        } else {
            res.sendStatus(404);
        }
    });
});
module.exports = router;
