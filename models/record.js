"use strict";
module.exports = function(sequelize, DataTypes) {
  var Record = sequelize.define("Record", {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    type: { type: DataTypes.STRING, defaultValue: '' },
    resize: { type: DataTypes.INTEGER, defaultValue: 0 },
    platform: { type: DataTypes.STRING, defaultValue: '' },
    customer: { type: DataTypes.STRING, defaultValue: '' },
    orderId: { type: DataTypes.STRING, defaultValue: '' },
    name: { type: DataTypes.STRING, defaultValue: '' },
    catogery: { type: DataTypes.STRING, defaultValue: '' },
    thumb: { type: DataTypes.STRING, defaultValue: '' },
    quantity: { type: DataTypes.INTEGER, defaultValue: 1 },
    content: { type: DataTypes.STRING, defaultValue: '' },
    material: { type: DataTypes.STRING, defaultValue: '' },
    orderDate: { type: DataTypes.DATE },
    recieveDate: { type: DataTypes.DATE },
    express: { type: DataTypes.STRING, defaultValue: '' },
    recieveId: { type: DataTypes.STRING, defaultValue: '' },
    sendDate: { type: DataTypes.DATE },
    sendId: { type: DataTypes.STRING, defaultValue: '' },
    address: { type: DataTypes.STRING, defaultValue: '' },
    reason: { type: DataTypes.STRING, defaultValue: '' },
    memo: { type: DataTypes.STRING, defaultValue: '' },
    statusCode: { type: DataTypes.STRING, defaultValue: '1' },
    zip: { type: DataTypes.STRING, defaultValue: '' },
    operationRecord: { 
      type: DataTypes.STRING(2048),
      defaultValue: '[]',
      get: function() { return JSON.parse(this.getDataValue('operationRecord')) },
      set: function(json) { this.setDataValue('operationRecord', JSON.stringify(json)); }
    },
    sizeImages: { 
      type: DataTypes.STRING(2048),
      defaultValue: '[]',
      get: function() { return JSON.parse(this.getDataValue('sizeImages')) },
      set: function(json) { this.setDataValue('sizeImages', JSON.stringify(json)); }
    },
    tags: { 
      type: DataTypes.STRING(2048),
      defaultValue: '[]',
      get: function() { return JSON.parse(this.getDataValue('tags')) },
      set: function(json) { this.setDataValue('tags', JSON.stringify(json)); }
    }
  })

  return Record 
};
